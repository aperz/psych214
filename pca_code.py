""" PCA exercise
"""

#: import common modules
import numpy as np  # the Python array package
import matplotlib.pyplot as plt  # the Python plotting package
# Display array values to 6 digits of precision
np.set_printoptions(precision=4, suppress=True)

#: import numpy.linalg with a shorter name
import numpy.linalg as npl
import nibabel

#- Load the image 'ds114_sub009_t2r1.nii' with nibabel
#- Get the data array from the image
data_file = nibabel.load("data/ds114_sub009_t2r1.nii")
data = data_file.get_fdata()

#- Make variables:
#- 'vol_shape' for shape of volumes
#- 'n_vols' for number of volumes
vol_shape = data.shape[:3]
n_vols = data.shape[3]

#- Slice the image data array to give array with only first two
#- volumes
data_s = data[:,:,:, :2]

#- Set N to be the number of voxels in a volume
N = data.shape[0]*data.shape[1]*data.shape[2]

#- Reshape to 2D array with first dimension length N
data3 = data_s.reshape(N,2)

#- Transpose to 2 by N array
data4 = data3.T

#- Calculate the mean across columns
means = data4.mean(axis=1)[:, np.newaxis]

#- Row means copied N times to become a 2 by N array

#- Subtract the means for each row, put the result into X
#- Show the means over the columns, after the subtraction
X = data4-means


#- Plot the signal in the first row against the signal in the second
plt.figure()
plt.scatter(X[0,:], X[1,:])
plt.savefig("figures/pca1.png")
plt.close()


#- Calculate unscaled covariance matrix for X
#C = np.cov(X)
ucv = X.dot(X.T)

#- Use SVD to return U, S, VT matrices from unscaled covariance
U,S,VT = npl.svd(ucv)

#- Show that the columns in U each have vector length 1 (unit vectors)
def vec_len(v, axis=1):
    """
    v is a np.array object
    """
    return np.sqrt(np.sum(v**2, axis=axis))

vec_len(U)

#- Confirm orthogonality of columns in U
U[:,0].dot(U[:,1]) # should be 0

#- Confirm tranpose of U is inverse of U
np.allclose(npl.inv(U), U.T)

#- Show the total sum of squares in X
#- Is this (nearly) the same as the sum of the values in S?

# S is variance explained by each component
np.allclose((X**2).sum(), S.sum())



#- Plot the signal in the first row against the signal in the second
#- Plot line corresponding to a scaled version of the first principal component
#- (Scaling may need to be negative)

plt.figure()
plt.plot(X[0,:], X[1,:], "+")
plt.plot([0,U[0,0]*-5000], [0,U[0,1]*-5000], "r")
plt.savefig("figures/pca2.png")
plt.close()


#- Calculate the scalar projections for projecting X onto the
#- vectors in U.
#- Put the result into a new array C.
C = U.dot(X)

#- Transpose C
#- Reshape the first dimension of C to have the 3D shape of the
#- original data volumes.
data_pca = C.T.reshape(data_s.shape)

#- Break 4D array into two 3D volumes

#- Show middle slice (over third dimension) from scalar projections
#- for first component

plt.figure()
plt.imshow(data_pca[:,:,14,0], cmap="gray")
plt.savefig("figures/pca3.png")
plt.close()


#- Show middle slice (over third dimension) from scalar projections
#- for second component

plt.figure()
plt.imshow(data_pca[:,:,14,1], cmap="gray")
plt.savefig("figures/pca4.png")
plt.close()

plt.figure()
plt.imshow(data[:,:,14,1], cmap="gray")
plt.savefig("figures/pca5.png")
plt.close()


#- Reshape first dimension of whole image data array to N, and take
#- transpose
X = data.reshape((N,data.shape[-1])).T

#- Calculate mean across columns
#- Expand to (173, N) shape using np.outer
#- Subtract from data array to remove mean over columns (row means)
#- Put result into array X
X = X-np.outer(X.mean(axis=1), np.ones(N))

#- Calculate unscaled covariance matrix of X
ucv = X.dot(X.T)
U,S,VT = npl.svd(ucv)


#- Use subplots to make axes to plot first 10 principal component
#- vectors
#- Plot one component vector per sub-plot.
fig,axes = plt.subplots(10)
for i,ax in enumerate(axes):
    ax.plot(U[:,i])

plt.savefig("figures/pca6.png")
plt.close("all")

#- Calculate scalar projections for projecting X onto U
#- Put results into array C.
C = U.dot(X)

#- Transpose C
#- Reshape the first dimension of C to have the 3D shape of the
#- original data volumes.

#- Show middle slice (over third dimension) of first principal
#- component volume

#- Make the mean volume (mean over the last axis)
#- Show the middle plane (slicing over the third axis)

#- Show middle plane (slice over third dimension) of second principal
#- component volume

#- Show middle plane (slice over third dimension) of third principal
#- component volume

