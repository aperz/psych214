""" Correlation per voxel, in 2D
"""

#: Standard imports
import numpy as np
import matplotlib.pyplot as plt
import nibabel as nib

#- import events2neural from stimuli module
from stimuli import events2neural

#- Load the ds114_sub009_t2r1.nii image
data = nib.load("data/ds114_sub009_t2r1.nii").get_fdata()

#- Get the number of volumes in ds114_sub009_t2r1.nii
n_trs = data.shape[-1]

#: TR
TR = 2.5

#- Call events2neural to give on-off values for each volume
pred = events2neural(task_fname = "data/ds114_sub009_t2r1_cond.txt", tr = TR, n_trs = n_trs)

#- Drop the first 4 volumes, and the first 4 on-off values
D = data[:,:,:,4:]
p = pred[4:]

#- Calculate the number of voxels (number of elements in one volume)
V = np.prod(data.shape[:3])

#- Reshape 4D array to 2D array n_voxels by n_volumes
D = D.reshape(V,D.shape[-1])

#- Make a 1D array of size (n_voxels,) to hold the correlation values
C = np.zeros(V)

#- Loop over voxels filling in correlation at this voxel
for i,v in enumerate(D):
    C[i] = np.corrcoef(p,v)[1,0]

# some cannot be computed - insert 0's?


#- Or (much faster) use pearson_2d function

#- Reshape the correlations array back to 3D
C = C.reshape(data.shape[:3])

#- Check you get the same answer when selecting a voxel time course
#- and running the correlation on that time course.  One example voxel
#- could be the voxel at array coordinate [42, 32, 19]

#- Plot the middle slice of the third axis from the correlations array
plt.figure()
plt.imshow(C[:,:,14], cmap="gray")
plt.savefig("figures/correlation_2d_code1.png")
plt.close()



