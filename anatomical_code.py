""" Anatomical exercise
"""
#: Compatibility with Python 3
from __future__ import print_function  # print('me') instead of print 'me'
from __future__ import division  # 1/2 == 0.5, not 0

#- Our usual imports

import numpy as np
import matplotlib.pyplot as plt

#- Read file into list of float values
filename = "data/anatomical.txt"
with open(filename, "r") as f:
    img = f.readlines()

img = [float(i.strip()) for i in img]

#- How many pixel values?
P = len(img)

#- Find the size of a slice over the third dimension
K = 32
IJ = int(P/K)

#- Find candidates for I

#- Find candidate pairs for I, J
def factors(n):
    o = set([i for i in range(1,int(n**(1/2))+1) if n%i==0])
    o = set([(i,j) for i,j in zip(o, [int(n/i) for i in o])])
    return o

#- Try reshaping using some candidate pairs

def draw_all():
    for I,J in [(i,j) for i,j in factors(IJ) if i == 85]:
        img_a = np.array(img).reshape((I,J,K))
        sl = img_a[:,:,20]

        plt.figure()
        plt.imshow(sl)
        plt.savefig("figures/anatomical_{0}x{1}.png".format(I,J))
        plt.close()

# wrong hints: the correct dim is (85,312,32)

plt.close("all")
