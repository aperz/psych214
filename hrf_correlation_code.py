""" Predicted neural time course exercise
"""
#: compatibility with Python 2
from __future__ import print_function, division

#: Standard imports
import numpy as np
import matplotlib.pyplot as plt
# Print arrays to 4 decimal places
np.set_printoptions(precision=4, suppress=True)

figure_name = "figures/hrf_correlation{0}.png"

#- Load the image with nibabel.
import nibabel as nib
filename = "data/ds114_sub009_t2r1.nii"
img = nib.load(filename)
data = img.get_fdata()

#- Get the image data array, print the data shape.

cpaste
#: Read in stimulus file, return neural prediction
def events2neural(task_fname, tr, n_trs):
    """ Return predicted neural time course from event file

    Parameters
    ----------
    task_fname : str
        Filename of event file
    tr : float
        TR in seconds
    n_trs : int
        Number of TRs in functional run

    Returns
    -------
    time_course : array shape (n_trs,)
        Predicted neural time course, one value per TR
    """
    task = np.loadtxt(task_fname)
    if task.ndim != 2 or task.shape[1] != 3:
        raise ValueError("Is {0} really a task file?", task_fname)
    task[:, :2] = task[:, :2] / tr
    time_course = np.zeros(n_trs)
    for onset, duration, amplitude in task:
        onset, duration = int(onset),  int(duration)
        time_course[onset:onset + duration] = amplitude
    return time_course
--

#: TR for this run
tr = 2.5

#- Read the stimulus data file and return a predicted neural time
#- course.
#- Plot the predicted neural time course.
neural_prediction = events2neural("data/ds114_sub009_t2r1_cond.txt", tr, data.shape[-1])

#- Make new array excluding the first volume
data_no_0 = data[..., 1:]

#- Knock the first element off the neural prediction time series.
neural_prediction_no_0 = neural_prediction[1:]

#: subtracting rest from task scans
task_scans = data_no_0[..., neural_prediction_no_0 == 1]
rest_scans = data_no_0[..., neural_prediction_no_0 == 0]
difference = task_scans.mean(axis=-1) - rest_scans.mean(axis=-1)

#: showing slice 14 from the difference image
plt.figure()
plt.imshow(difference[:, :, 14], cmap='gray')
plt.savefig(figure_name.format("1"))

#- Get the values for (i, j, k) = (45, 43, 14) and every volume.
#- Plot the values (voxel time course).
vtc = data_no_0[45,43,14,:]

plt.figure()
plt.plot(range(len(vtc)), vtc)
plt.savefig(figure_name.format("2"))

#- Correlation of predicted neural time course with voxel signal time
#- course
np.corrcoef(neural_prediction_no_0, vtc)

#: import the gamma probability density function
from scipy.stats import gamma

cpaste
def mt_hrf(times):
    """ Return values for HRF at given times

    This is the "not_great_hrf" from the "make_an_hrf" exercise.
    Feel free to replace this function with your improved version from
    that exercise.
    """
    # Gamma pdf for the peak
    peak_values = gamma.pdf(times, 6)
    # Gamma pdf for the undershoot
    undershoot_values = gamma.pdf(times, 12)
    # Combine them
    values = peak_values - 0.35 * undershoot_values
    # Scale max to 0.6
    return values / np.max(values) * 0.6
--

#- Make a vector of times at which to sample the HRF
times_to_sample = np.arange(0,36, tr)
sampled_hrf = mt_hrf(times_to_sample)

#- Sample HRF at given times
#- Plot HRF samples against times
plt.figure()
plt.plot(times_to_sample, sampled_hrf)
plt.savefig(figure_name.format("3"))

#- Convolve predicted neural time course with HRF samples
# compute a matrix with shifted HRF responses
m = data_no_0.shape[-1]
S = np.zeros((m, m))
for t in range(m):
    S[t,:] = np.concatenate((np.zeros(t), sampled_hrf, np.zeros(m-t)))[:m]
# multiply by neural prediction vector
hrf_signal = neural_prediction_no_0.T.dot(S)

# use numpy to achieve convolution
hrf_signal2 = np.convolve(sampled_hrf, neural_prediction_no_0)[:m]


#- Remove extra tail of values put there by the convolution
# trimmed them before

#- Plot convolved neural prediction and unconvolved neural prediction
plt.figure()
plt.plot(range(m), hrf_signal)
plt.savefig(figure_name.format("_convolved_manual"))

plt.figure()
plt.plot(range(m), hrf_signal2)
plt.savefig(figure_name.format("_convolved_numpy"))

#- Correlation of the convolved time course with voxel time course
np.corrcoef(hrf_signal, vtc)

#- Scatterplot the hemodynamic prediction against the signal
plt.figure()
plt.plot(vtc, hrf_signal, "+")
plt.xlabel("Voxel signal")
plt.ylabel("Convolved HRF (prediction by neural activity)")
plt.savefig(figure_name.format("_HRF_vs_voxel"))



