""" Find the arteries exercise
"""

#: Our usual imports
import numpy as np  # Python array library
import matplotlib.pyplot as plt  # Python plotting library
import os

#: Import nibabel
import nibabel as nib

#- Use nibabel to load the image ds107_sub001_highres.nii
filename = "data/ds107_sub001_highres.nii"
img = nib.load(filename)

data = img.get_fdata()


i = 0
def deco_plot(f):
    def fwrap(*args, **kwargs):
        plt.figure()
        f(*args, **kwargs)
        exercise_name = "arteries"
        #i = 0 #TODO
        fname = "figures/{0}{1}.png".format(exercise_name, str(i))
        plt.savefig(fname)
        plt.close()
    return fwrap


pplot = deco_plot(plt.plot)
phist = deco_plot(plt.hist)
pimshow = deco_plot(plt.imshow)

#- Plotting some slices over the third dimension

def draw_slices(by=20):
    for i, sli in enumerate(range(0, data.shape[2], by)):
        sl = data[:,:,sli]
        plt.figure()
        plt.imshow(sl, cmap="gray")
        plt.savefig("figures/arteries_slice{0}.png".format(str(sli)))
        plt.close()


#- Here you might try plt.hist or something else to find a threshold

plt.figure()
plt.hist(data.ravel())
plt.savefig("figures/arteries2.png")
plt.close()

plt.figure()
plt.hist(data[:,:,60].ravel())
plt.savefig("figures/arteries3.png")
plt.close()

plt.figure()
plt.plot(sorted(data[:,:,60].ravel()))
plt.savefig("figures/arteries4.png")
plt.close()

#- Maybe display some slices from the data binarized with a threshold
plt.figure()
plt.imshow(data[:,:,30] > 220, cmap="gray")
plt.savefig("figures/arteries5.png")
plt.close()


#- Create a smaller 3D subvolume from the image data that still
#- contains the arteries

data_small = data[80:140, 80:140, 15:75]
#data_small = data[80:140, 80:140, 25:65]


#- Try a few plots of binarized slices and other stuff to find a good
phist(data_small.ravel(), bins = 30)
threshold = 200

#: Uncomment the next line after installing scikit-image
from skimage import measure
# compiling on WSL: not successful. Finish setting up Arch ><


#: This function uses matplotlib 3D plotting and sckit-image for
# rendering
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

def binarized_surface(binary_array):
    """ Do a 3D plot of the surfaces in a binarized image

    The function does the plotting with scikit-image and some fancy
    commands that we don't need to worry about at the moment.
    """
    # Here we use the scikit-image "measure" function
    verts, faces = measure.marching_cubes(binary_array, 0)
    fig = plt.figure(figsize=(10, 12))
    ax = fig.add_subplot(111, projection='3d')

    # Fancy indexing: `verts[faces]` to generate a collection of triangles
    mesh = Poly3DCollection(verts[faces], linewidths=0, alpha=0.5)
    ax.add_collection3d(mesh)
    ax.set_xlim(0, binary_array.shape[0])
    ax.set_ylim(0, binary_array.shape[1])
    ax.set_zlim(0, binary_array.shape[2])

