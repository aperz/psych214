
def deco_plot(func=None, *, suffix=0):
    def deco_func(func):
        @functools.wraps(func)
        def func_wrapper(*args, **kwargs):
            plt.figure()
            f(*args, **kwargs)
            exercise_name = "arteries"
            i = 8 #TODO
            fname = "figures/{0}{1}.png".format(exercise_name, str(i))
            plt.savefig(fname)
            plt.close()
            return func_wrapper

    if func is None:
        return fwrap
    else:
        return fwrap(func)
