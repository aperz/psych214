""" Least-squares regression exercise
"""
#: Import numerical and plotting libraries
import numpy as np
# Print to four digits of precision
np.set_printoptions(precision=4, suppress=True)
import numpy.linalg as npl
import matplotlib.pyplot as plt

#: The data, that we are trying to model.
psychopathy = np.array([ 11.914,   4.289,  10.825,  14.987,
                         7.572,   5.447,   17.332,  12.105,
                         13.297,  10.635,  21.777,  20.715])

#: The regressor that we will use to model the data.
clammy = np.array([ 0.422,  0.406,  0.061,  0.962,  4.715,
                    1.398,  1.952,  5.095, 8.092,  5.685,
                    5.167,  7.257])

#- Create X design matrix fron column of ones and clammy vector
#X = np.vstack([np.ones_like(clammy), clammy-np.mean(clammy)]).T
X = np.vstack([np.ones_like(clammy), clammy]).T
y = psychopathy#-np.mean(psychopathy)

#- Check whether the columns of X are orthogonal
X.T.dot(X) # diagonal cells != 0

#- Check whether X.T.dot(X) is invertible
assert npl.det(X.T.dot(X)) != 0, "X.T.dot(X) is not ivertible"

#- Calculate (X.T X)^-1 X.T (the pseudoinverse)
X_pinv = npl.inv(X.T.dot(X)).dot(X.T)
X_pinv.shape
# it is of shape X.T.shape

#- Calculate least squares fit for beta vector
# y= XB + e
# X.T y = X.T X B + X.T e
# X.T X B = X.T y
# B = (X.T X)^-1 X.T y
B = npl.pinv(X.T.dot(X)).dot(X.T).dot(y)

#- Calculate the fitted values
fitted = X.dot(B)

#- mean of residuals near zero
np.mean(y-fitted)

#- Residuals orthogonal to design
# i.e. residuals vector is orthogonal to all columns in design
resids = y-fitted
X.T.dot(resids)


#- Copy X to new array X_o
X_o = X.copy()

#- Make second column orthogonal to first. Confirm orthogonality
X_o[:,1] = X_o[:,1] - X_o[:,1].mean()

npl.inv(X_o.T.dot(X_o))



""" What is the relationship between the values on the diagonal of
X_o.T.dot(X_o) and the lengths of the vectors in the first and second
columns of X_o?

THe values on the diagonal are equal to the norms of the vectors squared.
"""



""" What is the relationship between the values on the diagonal of the
*inverse* of X_o.T.dot(X_o) and the lengths of the vectors in the first
and second columns of X_o?

They are 1/x where x is the norm of the vector squared.
"""


#- Make mean-centered version of psychopathy vector
y_c = y-y.mean()

#- Calculate fit of X_o to y_o
B_o = npl.pinv(X_o.T.dot(X_o)).dot(X.T).dot(y_c)


""" Explain the new value of the first element of the parameter estimate
vector.

Because the variables were mean-centered, the intercept == 0.
"""


#- Correlation coefficient of y_c and the second column of X_o
r_xy = np.corrcoef(y_c, X_o[:,1])


""" What is the relationship between the correlation coefficient "r_xy"
and the second element in the parameter vector "B_o[1]"?

r_xy = 1/n * v1_standardized * v2_standardized
r_xy = (v1*v2)/norm(v1)*norm(v2)
where
v2 = B_o[:,1]
"""

#- Fit X_o to psychopathy data
B2 = npl.pinv(X_o.T.dot(X_o)).dot(X_o.T).dot(y)
fitted_o = X_o.dot(B2)


""" Explain the relationship between the mean of the psychopathy values
and the first element of the parameter estimate vector.

It equals the mean: it's where the correlation line crosses the y-axis.
"""



""" Why is the second value in B_o the same when estimating against "y_c"
and "psychopathy"?

Because the sd of the data hasn't changed.
"""


