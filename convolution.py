
import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt
from scipy.stats import gamma

figname = "figures/convolution{0}.png"

times = np.arange(0, 40, 0.1)
n_time_points = len(times)
neural_signal = np.zeros(n_time_points)
neural_signal[(times >= 4) & (times < 9)] = 1

def hrf(t):
    "A hemodynamic response function"
    return t ** 8.6 * np.exp(-t / 0.547)

# this is a response to an event occuring at time 0 of the response and
# lasting time len(hrf_signal != 0)
bold_times = np.arange(0, 20, 0.1)
bold_signal = hrf(bold_times)
n_bold_points = len(bold_signal)

plt.figure()
plt.plot(bold_times, bold_signal)
plt.xlabel('time (seconds)')
plt.ylabel('BOLD signal')
plt.title('Estimated BOLD signal for event at time 0')
plt.savefig(figname.format("_BOLD"))

# for several neural impulses of differing strength
neural_signal = np.zeros(n_time_points)
neural_signal[(times == 4)] = 1
neural_signal[(times == 10)] = 2
neural_signal[(times == 20)] = 3


cpaste
hrf_signal = np.zeros(n_time_points)
i_time = np.where(neural_signal != 0)[0]

plt.figure()

for t in i_time:
    strength = neural_signal[t]
    calc_response = bold_signal*strength
    replacement = hrf_signal[t:t+n_bold_points] + calc_response
    hrf_signal[t:t+n_bold_points]  = replacement

    addition = np.zeros(n_time_points)
    addition[t:t+n_bold_points] = np.zeros(n_time_points)[t:t+n_bold_points] + calc_response
    plt.plot(times, addition, label="time: "+str(t), linestyle = "dashed")

plt.plot(times, hrf_signal, label = "HRF", linestyle="dashed")
plt.plot(times, neural_signal*50, label = "neural", linestyle="dashed")

plt.xlabel('time (seconds)')
plt.ylabel('BOLD signal')
plt.legend()
plt.savefig(figname.format("_BOLD2"))
--

# for several neural impulses of differing strengths and lengths
#FIXME: length of the vector (trim output vector to n_time_points)
neural_signal = np.zeros(n_time_points)
neural_signal[(times >= 4) & (times<10)] = 1
neural_signal[(times == 20)] = 3


cpaste
hrf_signal = np.zeros(n_time_points)
i_time = np.where(neural_signal != 0)[0]

plt.figure()

for t in i_time:
    strength = neural_signal[t]
    calc_response = bold_signal*strength
    replacement = hrf_signal[t:t+n_bold_points] + calc_response
    hrf_signal[t:t+n_bold_points]  = replacement

    addition = np.zeros(n_time_points)
    addition[t:t+n_bold_points] = np.zeros(n_time_points)[t:t+n_bold_points] + calc_response

plt.plot(times, hrf_signal, label = "HRF", linestyle="dashed")
plt.plot(times, neural_signal*2000, label = "neural", linestyle="dashed")

plt.xlabel('time (seconds)')
plt.ylabel('BOLD signal')
plt.legend()
plt.savefig(figname.format("_BOLD3"))
--

# ====
# make shifted HRF matrix
# matrix with all the possible shifts of the HRF signal

times = np.arange(0, 40, 1)
n_time_points = len(times)

neural_signal = np.zeros(n_time_points)
neural_signal[(times >= 4) & (times<10)] = 1
neural_signal[(times == 20)] = 3

bold_times = np.arange(0, 20, 1)
bold_signal = hrf(bold_times)
n_bold_points = len(bold_signal)
M = n_time_points+n_bold_points-1

cpaste
shifted_matrix = np.zeros((n_time_points, M))

for t in range(n_time_points):
    shifted = np.concatenate(
        (np.zeros(t), bold_signal, np.zeros(n_time_points-t))
        )[:M] # note: it's better to just clip to len(neural_signal), since the rest is not needed really
    shifted_matrix[t,:] = shifted
--


plt.figure()
plt.imshow(shifted_matrix)
plt.savefig(figname.format("_shifted_matrix"))


# multiplying by the neural signal

#hrf_signal = neural_signal.reshape(-1,1).T.dot(shifted_matrix)
hrf_signal = shifted_matrix.T.dot(neural_signal.reshape(-1,1))

plt.figure()
plt.plot(times, hrf_signal[:len(neural_signal)], label = "HRF", linestyle="dashed")
plt.plot(times, neural_signal*50, label = "neural", linestyle="dashed")
plt.xlabel('time (seconds)')
plt.ylabel('BOLD signal')
plt.legend()
plt.savefig(figname.format("_BOLD4"))


# vs. auto-correlation

plt.figure()
plt.plot(range(shifted_matrix.shape[0]), shifted_matrix[:,25])
plt.savefig(figname.format("_col_is_reverse_row"))


