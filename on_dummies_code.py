""" Modeling groups with dummy variables exercise
"""
#: Import numerical and plotting libraries
import numpy as np
# Print to four digits of precision
np.set_printoptions(precision=4, suppress=True)
import numpy.linalg as npl
import matplotlib.pyplot as plt

#: Psychopathy scores from UCB students
ucb_psycho = np.array([2.9277, 9.7348, 12.1932, 12.2576, 5.4834])

#: Psychopathy scores from MIT students
mit_psycho = np.array([7.2937, 11.1465, 13.5204, 15.053, 12.6863])

#: Concatenate UCB and MIT student scores
psychopathy = np.concatenate((ucb_psycho, mit_psycho))

#- Create design matrix for UCB / MIT ANOVA
X = np.stack((
        np.ones_like(psychopathy),
        np.array([1,1,1,1,1,0,0,0,0,0]),
        np.array([0,0,0,0,0,1,1,1,1,1]),
        )).T
colnames = ["intercept", "ucb", "mit"]
y = psychopathy

#- Calculate transpose of design with itself.
#- Are the design columns orthogonal?
XtX = X.T.dot(X)
# The design columns are orthogonal.

#- Calculate inverse of transpose of design with itself.
iXtX = npl.pinv(XtX)


""" What is the relationship of the values on the diagonal of the inverse
of X.T.dot(X) and the number of values in each group?

The values on the diagonal of X.T.dot(X) are the number of values in each group.
The values on the diagonal of inv(X.T.dot(X)) are?
"""


#- Calculate transpose of design matrix multiplied by data
p = X.T.dot(y)


"""
What is the relationship of each element in this
vector to the values of ``ucb_psycho`` and ``mit_psycho``?

The elements in this vector are the means in each of the two groups multiplied
by the number of elements in a group.
"""


#- Calculate beta vector
B = iXtX.dot(p)

#- Compare beta vector to means of each group
B
ucb_psycho.mean()
mit_psycho.mean()
#np.mean(X[:,2].dot(y)/5)


r""" Using your knowledge of the parts of inv(X.T X) X y, explain the
relationship of the values in the estimated beta vector to the means of of
``ucb_psycho`` and ``mit_psycho``.

The values of beta hat are components of the respective means.
B[0] is the common component of the means?
np.allclose(np.sum(B[:2]), np.mean(X[:,1].dot(y)/5))
"""


# H0: mean psychopathy score for MIT students, μMIT, is higher than the
# mean psychopathy score for Berkeley students, μBerkeley

#- Contrast vector to express difference between UCB and MIT
#- Resulting value will be high and positive when MIT students have
#- higher psychopathy scores than UCB students
c = np.array([0,0,1])
top_t = c.T.dot(B)


#- Calculate the fitted and residual values
y_hat = X.dot(B)
e = y-y_hat

#- Calculate the degrees of freedom consumed by the design
df_design = npl.matrix_rank(X)
#- Calculate the degrees of freedom of the error
df_error = y.shape[0] - npl.matrix_rank(X) # n-p

#- Calculate the unbiased variance estimate
# sigma2_hat_biased = np.sum(e**2)
sigma2_hat = e.T.dot(e) / df_error

#- Calculate c (X.T X)^-1 c.T
bot_t1 = c.dot(iXtX).dot(c.T)


""" What is the relationship of ``c.T (X.T.X)^-1 c`` to ``p`` - the number
of observations in each group?
"""


#- Use scipy.stats to test if your t-test value is significant.
from scipy.stats import t,norm

rv = t(df = df_error)
t_crit = rv.ppf(.95) # one-sided: testing whether mu_mit > mu_ucb with alpha = 0.05

t_calc = top_t / np.sqrt(bot_t1*sigma2_hat)
# t_calc>t_crit > reject H0 > mu_mit>mu_ucb



""" Now imagine your UCB and MIT groups are not of equal size.  The total
number of students ``n`` has not changed. Call ``b`` the number of
Berkeley students in the ``n=10``, where ``b in range(1, 10)``.  Write the
number of MIT students as ``n - b``.  Using your reasoning from the equal
group sizes case above, derive a simple mathematical formula for the
result of ``c.dot(npl.inv(X.T.dot(X)).dot(c)`` in terms of ``b`` and
``n``. ``c`` is the contrast vector you chose above.  If all other things
remain equal, such as the sigma estimate and the top half of the t
statistic, then what value of ``b`` should you chose to give the largest
value for your t statistic?
"""


#: Clamminess of handshake for UCB and MIT students
clammy = np.array([2.6386, 9.6094, 8.3379, 6.2871, 7.2775, 2.4787,
                   8.6037, 12.8713, 10.4906, 5.6766])


"""
Make the alternative full model X_f. Compute the extra degrees of
freedom nu_1.  Compute the extra sum of squares and the F statistic.
"""

Xf = np.column_stack((X,clammy))
Xf_colnames = colnames + ["clammy"]
Xr = X

# F_calc = [(SSR_r - SSR_f)/df1 ] / [SSR_f / df2]
# df1 (nu1) = np.matrix_rank(Xf) - np.matrix_rank(Xr) # no of extra regressors
df1 = npl.matrix_rank(Xf) - npl.matrix_rank(Xr)
# df2 (nu2) = n - np.matrix_rank(Xf) # df of error
df2 = Xf.shape[0] - npl.matrix_rank(Xf)

# sigma2_hat = SSR / df # non-biased estimate
Bf = npl.pinv(Xf.T.dot(Xf)).dot(Xf.T).dot(y)
Br = npl.pinv(Xr.T.dot(Xr)).dot(Xr.T).dot(y)

y_hat_f = Xf.dot(Bf)
y_hat_r = Xr.dot(Br)
SSR_f = np.sum((y - y_hat_f)**2)
SSR_r = np.sum((y - y_hat_r)**2)

F_calc = ((SSR_r - SSR_f)/df1) / (SSR_f / df2)

from scipy.stats import f
rv = f(dfn = df2, dfd = df1)
F_crit = rv.ppf(0.95)







