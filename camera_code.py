""" Cameraman exercise
"""
#: Compatibility with Python 2
from __future__ import print_function  # print('me') instead of print 'me'
from __future__ import division  # 1/2 == 0.5, not 0

#- Our usual imports
import numpy as np
import matplotlib.pyplot as plt

#- Read lines from file and convert to list of floats
filename = "data/camera.txt"
with open(filename, "r") as f:
    img = f.readlines()

img = [float(i.strip()) for i in img]
P = len(img)

#- Guess M, N
def factors(n):
    o = set([i for i in range(1,int(n**(1/2))+1) if n%i==0])
    o = set([(i,j) for i,j in zip(o, [int(n/i) for i in o])])
    return o

# check out reduce (and functools in general)
#https://stackoverflow.com/questions/6800193/what-is-the-most-efficient-way-of-finding-all-the-factors-of-a-number-in-pythonhttps://stackoverflow.com/questions/6800193/what-is-the-most-efficient-way-of-finding-all-the-factors-of-a-number-in-python
from functools import reduce

def factors2(n):
    return set(reduce(list.__add__,
            ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))

M = 512
N = 512

#- Convert list to array and reshape
img_a = np.array(img).reshape(M,N)


#- Show image using plt module
plt.figure()
plt.imshow(img_a)
plt.savefig("figures/camera1.png")

#- A nicer version of the original plot
plt.figure()
plt.imshow(img_a.T, cmap="gray")
plt.savefig("figures/camera2.png")


#- A plot of the pixel position in x and the pixel value in y, for an image line.
plt.figure()
plt.plot(np.arange(0, len(img)), sorted(img))
plt.savefig("figures/camera3.png")

#- Apply threshold to make new binary image, and show binary image
mask = img_a > 0.4
plt.figure()
plt.imshow(mask.T, cmap="gray")
plt.savefig("figures/camera4.png")

#- Extra points - a good image of the man's pocket.
plt.figure()
plt.imshow(np.clip(img,0.0,0.1).reshape(img_a.shape).T, cmap="gray")
plt.savefig("figures/camera5.png")

plt.close("all")
