""" First go at brain activation exercise
"""

#: import common modules
import numpy as np  # the Python array package
import matplotlib.pyplot as plt  # the Python plotting package
# Display array values to 6 digits of precision
np.set_printoptions(precision=4, suppress=True)

#- Read the file into an array called "task".
#- "task" should have 3 columns (onset, duration, amplitude)
#- HINT: np.loadtxt
task = np.loadtxt("data/ds114_sub009_t2r1_cond.txt")#, columns = ["onset", "duration", "amplitude"])
tr = 2.5

#- Select first two columns and divide by TR
task[:,0:2] = task[:,0:2]/tr

#- Load the image and check the image shape to get the number of TRs
data = nib.load("data/ds114_sub009_t2r1.nii").get_fdata()

#- Make new zero vector
time_course = np.array([bool(i) for i in (np.zeros(data.shape[-1]))])

#: try running this if you don't believe me
len(range(4, 16))

#- Fill in values of 1 for positions of on blocks in time course
for ev in task:
    time_course[int(ev[0]):int(ev[0]+ev[1])] = 1

#- Plot the time course
plt.figure()
plt.plot(time_course)
plt.savefig("figures/first_activation1.png")
plt.close()

#- Make two boolean arrays encoding task, rest


#- Create a new 4D array only containing the task volumes
taskv = data[:,:,:,time_course]

#- Create a new 4D array only containing the rest volumes
restv = data[:,:,:,~time_course]

#- Create the mean volume across all the task volumes.
#- Then create the mean volume across all the rest volumes

#- Create a difference volume
diff = taskv.mean(axis=3) - restv.mean(axis=3)

#- Show a slice over the third dimension
plt.figure()
plt.imshow(diff[:,:,14], cmap="gray")
plt.savefig("figures/first_activation2.png")
plt.close()


#- Calculate the SD across voxels for each volume
#- Identify the outlier volume
rest_std = restv.std(axis=(0,1,2))
outlier = rest_std == rest_std.max()

#- Use slicing to remove outlier volume from rest volumes
restv = restv[:,:,:,~outlier]

#- Make new mean for rest volumes, subtract from task mean
diff = taskv.mean(axis=3) - restv.mean(axis=3)

#- show same slice from old and new difference volume
plt.figure()
plt.imshow(diff[:,:,14], cmap="gray")
plt.savefig("figures/first_activation3.png")
plt.close()


# ADDITION: outlier detection
# 1. fit a normal to rest volume std's:
from scipy.stats import norm
mu, std = norm.fit(rest_std)
Z = (rest_std-mu)/std # sigma
#restv = restv[:,:,:,Z > 1.96]

