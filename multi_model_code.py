""" Basic linear modeling
"""
#: Import some standard librares
import numpy as np
# Print to 4 DP
np.set_printoptions(precision=4)
import numpy.linalg as npl
import matplotlib.pyplot as plt
# Set default imshow parameters
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

#: Load the image as an image object
import nibabel as nib
img = nib.load('data/ds114_sub009_t2r1.nii')
figure_name = "figures/multi_model{0}.png"

#: Load the image data as an array
# Drop the first 4 3D volumes from the array
# (We already saw that these were abnormal)
data = img.get_fdata()[..., 4:]

#- Load the pre-written convolved time course
#- Knock off the first four elements
convolved = np.loadtxt('data/ds114_sub009_t2r1_conv.txt')[4:]
convolved = convolved.reshape(-1,1)

#- Compile the design matrix
#- First column is convolved regressor
#- Second column all ones
X = np.hstack((convolved, np.ones_like(convolved)))
#- Hint: investigate "aspect" keyword to ``plt.imshow`` for a nice
#- looking image.
plt.figure()
plt.imshow(X, aspect=0.2)
plt.savefig(figure_name.format("_design"))

#- Reshape the 4D data to voxel by time 2D
M = data.shape[0]*data.shape[1]*data.shape[2]
Y = data.reshape(M, data.shape[3])

#- Transpose to give time by voxel 2D
Y = Y.T
#- Calculate the pseudoinverse of the design
piX = npl.pinv(X)
#- Apply to time by voxel array to get betas
beta_hat = piX.dot(Y) # models each voxel

#- Transpose betas to give voxels by 2 array
#- Reshape into 4D array, with same 3D shape as original data,
#- last dimension length 2
fitted = beta_hat.T.reshape(data.shape[0], data.shape[1], data.shape[2], -1)

#- Show the middle slice from the first beta volume
plt.figure()
plt.imshow(fitted[:,:,14,0])
plt.savefig(figure_name.format("_fitted1"))

#- Show the middle slice from the second beta volume
plt.figure()
plt.imshow(fitted[:,:,14,1])
plt.savefig(figure_name.format("_fitted2"))
