""" Basic numpy exercises
"""
#: Compatibility with Python 2
from __future__ import print_function  # print('me') instead of print 'me'
from __future__ import division  # 1/2 == 0.5, not 0

import numpy as np

#- create array "a" with values
#-     2  7 12  0
#-     3  9  3  4
#-     4  0  1  3

a = np.array([[2, 7, 12, 0],
    [3, 9, 3, 4],
    [4, 0, 1, 3]])

#- Array shape?
a.shape

#- Array ndim?
a.ndim

#- Array length
len(a)

#- Get ndim and length from the shape
ndim = len(a.shape)
length = a.shape[0]

#- 1D array 2 through 5
np.arange(2,5)

#- 10 equally spaced elementd between 2 and 5
np.linspace(2,5,10)


#- Shape 4,4 array of 1
np.ones((4,4))

#- Identity array shape 6, 6
np.identity((6,6))


#- Array with top left value == 1 etc
np.diag([1,2,3])

#- Array of random numbers shape 3, 5
#np.array([[np.random.random() for i in range(3*5)]]).reshape((3,5))
np.random.rand(3,5)

#- x is an array with 100 evenly spaced numbers 0 - 2 pi
x = np.linspace(0,2*np.pi, 100)

#- y has cosines of values in x
y = np.cos(x)

#- plot x against y
import matplotlib.pyplot as plt
plt.figure()
plt.plot(x,y)
plt.savefig("figures/fig1.png")

#- Shape 10, 20 array of random numbers
b = np.random.rand(10,20)

#- Display as image
plt.figure()
plt.imshow(b)
plt.savefig("figures/fig2.png")


#- Grayscale image of array
plt.figure()
plt.imshow(b, cmap="gray")
plt.savefig("figures/fig3.png")


#- Create array "a"

#- 2nd row of a
a[1,:]

#- 3rd column of a
a[:,2]

#- Build given arrays


#- Use np.tile to construct array
np.tile(np.arange(4,0, -1).reshape(2,-1), (2,3))

#- Create array a

#- Make mask for values greater than 5
mask = a > 5

#- Return all values in a that are greater than 5
a[mask]
#a[~mask]

#- Set all elements greater than 5 to equal 5
a[mask] = 5

#- Add even and odd columns of a
odc = a[:,::2]
evc = a[:,1::2]
odc+evc

#- Generate array of powers of 2
np.array([2**i for i in range(0,4)])
2**np.arange(0,4)

#- Generate array
# x[i] = 2 ** (3 * i) - i
2**(3*np.linspace(0,1,10)) - np.linspace(0,1,10)


#- Sum of values in a

#- Sum of the values of the columns in a
a.sum(0)

#- Sum of the values of the rows in a
a.sum(1)

#- Mean of all the values in a
a.mean()

#- Minimum of all the values in a
a.min()

#- Maximum of all the values in a
a.max()
