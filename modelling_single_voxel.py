#: Standard imports
import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt
# Print arrays to 4 decimal places
np.set_printoptions(precision=4, suppress=True)

figure_name = "figures/modelling_single_voxel{0}.png"

#- Load the image with nibabel.
import nibabel as nib
filename = "data/ds114_sub009_t2r1.nii"
img = nib.load(filename)
data = img.get_fdata()
data = data[..., 4:]
voxel_time_course = data[42, 32, 19]

plt.figure()
plt.plot(voxel_time_course)
plt.savefig(figure_name.format("1"))

convolved = np.loadtxt('data/ds114_sub009_t2r1_conv.txt')[4:]
convolved = convolved.reshape(-1,1)

plt.figure()
plt.plot(convolved)
plt.savefig(figure_name.format("2"))

"""
Trying to understancd what's going on.

The voxel_time_course is the real observed data (Y)
The convolved is the explanatory variable (X). It is basically the equivalent
of the neural signal data. WHat about predicting voxel_time_course with just
the neural data (which is also the event data)? Why not?
Is it because it's only a 0-1 thing?

We're trying to model the empirical response using a vector of convolved data.
"""

# make a design matrix with a column for the convolved regressor and a col of ones
X = np.concatenate((np.ones_like(convolved), convolved), axis=1)
Y = voxel_time_course.reshape(-1,1)

# calculate coefficients
B_hat = npl.pinv(X).dot(Y)

Y_hat = X.dot(B_hat)
e = Y_hat-Y # errors, residuals

# plot
plt.figure()
plt.plot(Y, label = "data")
plt.plot(Y_hat, label = "prediction")
plt.title("Voxel time course data vs. prediction with a convolved vector.")
plt.legend()
plt.savefig(figure_name.format("_voxel_data_vs_prediction"))


# ===========
# TESTING
# ===========

# have estimates of beta
# want to test if beta != 0?

# get sigma_hat (unbiased var estimate)
RSS = np.sum(e**2)
df = X.shape[0] - npl.matrix_rank(X)
sigma2_hat = RSS/df # = MRSS

# get the var-covar matrix for beta
C = sigma2_hat*npl.pinv((X.T.dot(X)))

# get t value
# t = (mean_hat - mean_hypothesized) / sigma_hat # standard error
t = B_hat[0] / np.sqrt(C[0,0])

