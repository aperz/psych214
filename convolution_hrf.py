import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt
from scipy.stats import gamma

figname = "figures/convolution_hrf{0}.png"
# the gamma distribution
x = np.arange(0, 25, 0.1)

plt.figure()
plt.plot(x, gamma.pdf(x, 2), label="k=2")
plt.plot(x, gamma.pdf(x, 4), label="k=4")
plt.plot(x, gamma.pdf(x, 8), label="k=8")
plt.plot(x, gamma.pdf(x, 12), label="k=12")
plt.legend()
plt.savefig(figname.format("_gamma"))

# Our function will accept an array that gives the times we want to calculate the
# HRF for, and returns the values of the HRF for those times. We will assume that
# the true HRF starts at zero, and gets to zero sometime before 35 seconds.

times = np.arange(0,35,0.1)

def hrf(times):
    density1 = gamma.pdf(times, 4)
    density2 = gamma.pdf(times, 12)
    o = density1 - 0.35* density2 #/ np.sum(density1+density2)
    return o / np.max(o) * 0.6

#plt.plot(times, density1, label="d1")
#plt.plot(times, density2, label="d2")
plt.figure()
plt.plot(times, hrf(times), label="HRF")
plt.legend()
plt.savefig(figname.format("_hrf"))


# this is a response to a single event
times_tr = np.arange(0,30,2.5)

plt.figure()
plt.plot(times_tr, hrf(times_tr), label="HRF")
plt.legend()
plt.savefig(figname.format("_hrf_tr"))


# convolution
from stimuli import events2neural
n_vols = 173
TR = 2.5

neural_activity_prediction = events2neural('data/ds114_sub009_t2r1_cond.txt', TR, n_vols)
timepoints = np.arange(173)*TR
hrf_at_trs = hrf(times_tr)

convolved = np.convolve(neural_activity_prediction, hrf_at_trs)
len(neural_activity_prediction)
len(hrf_at_trs)
len(convolved)
convolved = convolved[:len(neural_activity_prediction)]

plt.figure()
plt.plot(timepoints, neural_activity_prediction, label="neural activity")
plt.plot(timepoints, convolved, label="HRF")
plt.legend()
plt.savefig(figname.format("_convolved"))

# Save the convolved time course to 6 decimal point precision
np.savetxt('data/ds114_sub009_t2r1_conv.txt', convolved, fmt='%2.6f')

